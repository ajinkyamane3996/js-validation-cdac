function ValidateEmpty(CtrlID, SpanID, ErrMsg) {
    var isITEmpty = true;
    var element = document.getElementById(CtrlID);
    var errspan = document.getElementById(SpanID);
    if (element.value == "") {
        errspan.innerHTML = ErrMsg;
        isITEmpty = true;
    }
    else {
        errspan.innerHTML = "";
        isITEmpty=false;
    }
    return isITEmpty
}
function ValidateForTextOnly(CtrlID, SpanID, ErrMsg) {
    var element = document.getElementById(CtrlID);
    var errspan = document.getElementById(SpanID);
    if (isNaN(element.value)) {
        errspan.innerHTML = "";
    }
    else {
        errspan.innerHTML = ErrMsg;
    }
}
function ValidateForNumberOnly(CtrlID, SpanID, ErrMsg) {
    var isItNumber = false;

    var element = document.getElementById(CtrlID);
    var errspan = document.getElementById(SpanID);

    if (isNaN(element.value)) {
        errspan.innerHTML=ErrMsg;
        isItNumber=false;
    } else {
        errspan.innerHTML="";
        isItNumber=true;
    }
}
function ValidateForRange(CtrlID, SpanID,ErrMsg,Min, Max)
{
    debugger;
    var element = document.getElementById(CtrlID);
    var errspan = document.getElementById(SpanID);
    var actualValue = parseInt(element.value)
    if (actualValue < Min || actualValue > Max) {
        errspan.innerHTML=ErrMsg;
    } else {
        errspan.innerHTML="";
    }
}